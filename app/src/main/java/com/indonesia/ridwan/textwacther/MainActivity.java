package com.indonesia.ridwan.textwacther;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText et_write;
    private TextView tv_display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_write=(EditText)findViewById(R.id.et_write);
        tv_display=(TextView)findViewById(R.id.tv_display);

        et_write.addTextChangedListener(textWatcher);

    }
    TextWatcher textWatcher = new TextWatcher(){



        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub
            if (tv_display.getText().length() > 6){
                Toast.makeText(getApplicationContext(),"Hore ",Toast.LENGTH_LONG).show();
            }
            tv_display.setText(s);
        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub

        }
    };
}
